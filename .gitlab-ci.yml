# Use default terraform:light container for all jobs unless otherwise specified.
image:
  name: hashicorp/terraform:light
  entrypoint:
    - '/usr/bin/env'
    - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'

# ------

# The stages mostly follow the Terraform lifecyle commands. The "verify_bdd" is
# added to test the Terraform plan using terraform-compliance -
# https://github.com/eerkunt/terraform-compliance Any BDD/testing
# tool could be used in this place, of if you are a HashiCorp enterprise
# customer then you could integrate their Sentinel product

stages:
  - docker builds
  - init
  - validate
  - plan
  - verify_bdd
  - apply
  - destroy

# ------

# The before_script runs before any job in any stage.

# If you will be writing / editing the CI YAML files then it's advised to bookmark
# this online reference: https://docs.gitlab.com/ee/ci/yaml/

# In the section below, we check to see if we have set a GitLab CI/CD variable
# called environment. If this is set, we will set the environment_path to equal
# this value ONLY if it is not empty. IF the environment variable is not passed
# then the script will set the path to what is in the repo - environments/default

# As an example, if you wanted to use the same project for a different "environment"
# then you'd commit that environment into the repo & set the environment variable
# in the CI/CD variables section in GitLab.

# Attribution on if/elif/else script:
# https://stackoverflow.com/questions/3601515/how-to-check-if-a-variable-is-set-in-bash

before_script:
  - echo $environment
  - >
    if [ ! -v environment ]; then
      export environment_path="environments/default"
    elif [ -z "$environment" ]; then
      export environment_path="environments/default"
    else
      export environment_path=$environment
    fi
# Based on the logic above, we'll ensure the working directory for terraform <command>
# is set:
  - cd $CI_PROJECT_DIR/$environment_path
# The project_path is used to minimize typing in later jobs..
  - export project_path=$CI_PROJECT_DIR/$environment_path
# A different approach would be to run a terraform init in the before_script


# ------

build:terra-py-compliance:
  stage: docker builds
  image: docker:latest
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
  services:
    - docker:dind
  script:
# Current thinking is to add build the container & tag with both the date
# in YYYY-mm-dd-h-m-s format as well as "latest":
    - timestamp=$(date +%Y%m%d%H%M%S)
    - cd $CI_PROJECT_DIR/
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build --no-cache docker/terra-py-compliance -t terra-py-compliance
    - docker tag terra-py-compliance registry.gitlab.com/$CI_PROJECT_PATH/terra-py-compliance:latest
    - docker tag terra-py-compliance registry.gitlab.com/$CI_PROJECT_PATH/terra-py-compliance:$timestamp
    - docker push registry.gitlab.com/$CI_PROJECT_PATH/terra-py-compliance:latest
    - docker push registry.gitlab.com/$CI_PROJECT_PATH/terra-py-compliance:$timestamp
# Only build the container if the Dockerfile is updated:
  only:
    changes:
        - docker/terra-py-compliance/**/*

build:selenium-chrome:
  stage: docker builds
  image: docker:latest
  variables:
    DOCKER_HOST: tcp://docker:2375/
    DOCKER_DRIVER: overlay2
  services:
    - docker:dind
  script:
# Current thinking is to add build the container & tag with both the date
# in YYYY-mm-dd-h-m-s format as well as "latest":
    - timestamp=$(date +%Y%m%d%H%M%S)
    - cd $CI_PROJECT_DIR/
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build --no-cache docker/selenium-chrome -t selenium-chrome
    - docker tag selenium-chrome registry.gitlab.com/$CI_PROJECT_PATH/selenium-chrome:latest
    - docker tag selenium-chrome registry.gitlab.com/$CI_PROJECT_PATH/selenium-chrome:$timestamp
    - docker push registry.gitlab.com/$CI_PROJECT_PATH/selenium-chrome:latest
    - docker push registry.gitlab.com/$CI_PROJECT_PATH/selenium-chrome:$timestamp
# Only build the container if the Dockerfile is updated:
  only:
    changes:
        - docker/selenium-chrome/**/*
# ------

# The tf init stage runs a terraform init & then uploads the results as an
# artifact into GitLab. By default, gitlab.com, does not expire artifacts; we
# will set the expiration to 1 month.

# Within GitLab, we use Terraform in pipelines. The init stage is .init in most
# examples. This will not show in the pipeline. However, to demonstrate the
# relationship between artifacts & Terraform commands, the script below is
# explictly showing how the sausage is made.

tf init:
  stage: init
  script:
    - terraform init
# The line below may be a bit curious. As of Gitlab 12.3, variables, such as,
# $environment_path are now allowed in artifact paths. This means that we need
# to figure out a way to upload the artifact directory nested under a variable.
# You'll see the workaround I came up with in the sections below.
# For clarity sake, variables I've created are lower case; variables provided
# by GitLab's CI are in upper case.
    - mv $project_path/.terraform $CI_PROJECT_DIR/
  artifacts:
    paths:
      - .terraform
# As noted above, artifacts can live forever on GitLab.com. I don't really see
# the point to that for this project as Terraform can rebuilt an environment
# as needed. So..
    expire_in: 1 month



# ------

# The tf validate stage downloads the .terraform directory into the newly cloned
# root directoy of the project. As our working directory is set in the before_script
# we'll want to ensure the .terraform directory is in the right location before
# running the "terraform validate" command. Once the validation is complete, we
# move the results back to the root dir so that the artifacts are consistent for
# later stages.

# tf validate runs a 'terraform validate'

tf validate:
  stage: validate
  extends: tf init
  script:
    - mv $CI_PROJECT_DIR/.terraform $project_path
    - terraform validate
    - mv $project_path/.terraform $CI_PROJECT_DIR/

# ------

# For plan, we'll need to track not only the .terraform directory but also the
# Terraform plan. We'll use the built in GitLab CI pipeline ID:
tf plan:
  stage: plan
  extends: tf init
  script:
    - mv $CI_PROJECT_DIR/.terraform $project_path
    - terraform plan -out $CI_PIPELINE_ID.tfplan
    - mv $project_path/.terraform $CI_PROJECT_DIR/
    - mv $project_path/$CI_PIPELINE_ID.tfplan $CI_PROJECT_DIR/
  dependencies:
    - tf validate
  artifacts:
    paths:
      - $CI_PIPELINE_ID.tfplan
      - .terraform
    expire_in: 1 month

# ------

# For the external testing tool, we'll use a custom container pulled from GitLab's
# container registry. The Dockerfile for this container can be found in
# $CI_PROJECT_DIR/Misc/Docker. The container uses a lightweight Python base & adds
# terraform-compliance (see previous note for URL) & the latest Terraform binary.
# The same logic applies from earlier stages - move the artifacts to the proper
# locations.
# In the example below, the testing is very limited & more to show HOW a testing
# framework could be incorporated.
Preflight checklist:
  stage: verify_bdd
  image:
    name:
      registry.gitlab.com/$CI_PROJECT_PATH/terra-py-compliance:latest
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
  script:
    - terraform --version
    - python --version
    - mv $CI_PROJECT_DIR/.terraform $project_path
    - mv $CI_PROJECT_DIR/$CI_PIPELINE_ID.tfplan $project_path/
    - terraform-compliance -f $CI_PROJECT_DIR/test/aws/ -p $project_path/$CI_PIPELINE_ID.tfplan
# You'll notice the introduction of the "only" directive added to the job below.
# The purpose of this is to test only on branches. If we test there then we do
# not need to test on master.
  only:
    - branches
# No artifacts; not needed as this stage & job reads the terraform plan \
# and tests against this only.
# Some other test frameworks, like test-kitchen, will deploy infra and test
# against what's deployed.

# ------

# tf apply's main goal is to run a "terraform apply" against the tested Terraform
# plan.

# tf apply runs 'terraform apply -input=false <pipeline id #>.tfplan'
# You'll note that apply & destroy are manual stages; they will not automatically
# fire


tf apply:
  stage: apply
  extends:
    - tf plan
  script:
    - mv $CI_PROJECT_DIR/.terraform $project_path/
    - mv $CI_PROJECT_DIR/$CI_PIPELINE_ID.tfplan $project_path/
    - terraform apply -input=false $CI_PIPELINE_ID.tfplan
    - mv $project_path/.terraform $CI_PROJECT_DIR/
    - mv $project_path/$CI_PIPELINE_ID.tfplan $CI_PROJECT_DIR/
  dependencies:
    - tf plan
  artifacts:
    paths:
      - .terraform
# We'll set the artifacts to go away after 6 months. If your infrastructure lasts
# for more than 6 months between runs then either remove this, if run on gitlab.com, or
# move the expiration out further.
    expire_in: 6 month
# We'll run this job ONLY if we're on the master branch or we're running against a tag (generally as a release) We don't want to change
# infrastructure on a feature branch.
  only:
    - master
    - tags
# We'll set the type of job to be manual vs. automated. This command sets a stage
# gate & follows the CD practice of Continuous Delivery vs. Continuous Deployment
  # when: manual

# ------

# tf destroy removes the infrastructure deployed in the previous stage.
# It's a run only against master & it's a manual job as well. If the manual job
# were not set then GitLab would deploy the infrastructure and then turn around
# and remove it.
# Another option considered woudl be to have the infrastructure expire in a
# finite amount of time. However, as of GitLab 12.3, the limit is set to 1 hour.
# Related issue: https://gitlab.com/gitlab-org/gitlab/issues/28890
tf destroy:
  stage: destroy
  extends:
    - tf apply
  script:
    - mv $CI_PROJECT_DIR/.terraform $project_path/
    - terraform destroy -auto-approve
  dependencies:
    - tf apply
  only:
    - master
    - tags
  when: manual

# ------
