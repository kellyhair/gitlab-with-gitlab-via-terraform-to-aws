
terraform {
  required_version = ">= 0.12"
  backend "s3" {
    bucket = "gitlab-bucket-kh"
    key    = "GitLab_CI_CD"
    region = "us-east-2"
    }
  }
#note ^^^ key hardcoded. Checking logic
